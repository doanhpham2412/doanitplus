<div class="page-wrapper">
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" method="post">
                        <div class="card-body">
                            <h4 class="card-title">Đổi mật khẩu</h4>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-5 text-right control-label col-form-label">Mật khẩu  (*) </label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="fname"  required name = "pass" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-5 text-right control-label col-form-label">Mật khẩu  mới  (*)</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="fname"  required name = "newpass" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-5 text-right control-label col-form-label">Nhập lại mật khẩu  (*) </label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="fname"  required name = "renewpass" placeholder="">
                                </div>
                            </div>

                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" name = "submit" class="btn btn-primary">Submit</button>

                            </div>
                        </div>
                    </form>
                </div>


            </div>

        </div>
        <!-- editor -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
</div>