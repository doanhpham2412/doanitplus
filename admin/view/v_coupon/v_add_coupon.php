<div class="page-wrapper">
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return submitCoupon()">
                        <div class="card-body">
                            <h4 class="card-title">Thêm khuyến mãi</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên khuyến
                                    mãi</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="ID" value="<?= $s_id ?>" name="ID"
                                           style="display: none">
                                    <input type="text" class="form-control" id="coupon_name" value="<?= $s_couponName ?>"
                                            name="coupon_name" placeholder="Tên khuyến mãi">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Áp dụng
                                    cho</label>
                                <div class="col-sm-9">
                                    <select class="select2 custom-select" style="width: 30%; height:36px;"
                                            name="id_type">
                                        <option value="">Chọn</option>

                                        <?php
                                        foreach ($types as $v) {
                                            echo '<option value="' . $v->ID . '">'. $v->ten_loai_san_pham . '</option>';
                                        }
                                        //
                                        //                                        ?>
                                    </select></div>
                            </div>

                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Phẩn trăm
                                    giảm giá</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="discount" name="discount"
                                           value="<?= $s_discount ?>" placeholder="Giảm giá %">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Ngày bắt
                                    đầu</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="date_start" name="date_start"
                                           value="<?= $s_dateStart ?>" >

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Ngày kết
                                    thúc</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="date_end" name="date_end"
                                           value="<?= $s_dateEnd ?>" >

                                </div>
                            </div>   <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Số lượng</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="amount" name="amount"
                                           value="<?= $s_amount ?>" >

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email1" class="col-sm-3 text-right control-label col-form-label">Trạng
                                    thái</label>
                                <div class="col-sm-9">
                                    <select name="status" class="select2 custom-select" style="width: 30%; height:36px;" >
                                        <option value="1" <?php echo $s_status == 1 ? 'selected' : ''; ?>>Mở</option>
                                        <option value="0" <?php echo ($s_status == 0 && $s_status != "") ? 'selected' : ''; ?>>
                                            Khóa
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button  name="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>

        </div>
        <!-- editor -->

    </div>
</div>