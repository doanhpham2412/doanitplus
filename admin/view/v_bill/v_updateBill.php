<div class="page-wrapper">
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form  class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Cập nhật hóa đơn</h4>
                            <?php foreach ($bill as $key => $value){ ?>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">ID Hóa Đơn</label>
                                <div class="col-sm-9">
                                    <input  class="form-control" name="id_hoa_don" value="<?php echo $value->ID;?>"  readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên Khách Hàng</label>
                                <div class="col-sm-9">
                                    <input  class="form-control" value="<?php echo $value->ten_khach_hang;?>"  readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Hình Thức Thanh Toán</label>
                                <div class="col-sm-9">
                                    <input  class="form-control" value="<?php echo $value->hinh_thuc_thanh_toan;?>"  readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Trạng Thái</label>
                                <div class="col-md-9">
                                    <select name="trang_thai" class="select2 form-control custom-select" style="width: 100%; height:36px;">
                                        <option value="0" <?php echo ($value->trang_thai == 0)?'selected':''?> >Chưa thanh toán</option>
                                        <option value="1" <?php echo ($value->trang_thai == 1)?'selected':''?>>Đã Thanh Toán</option>
                                    </select>
                                </div>
                            </div>
                            <?php
                                break;
                            }
                            ?>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" name = "submit" class="btn btn-primary">Cập Nhật</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
