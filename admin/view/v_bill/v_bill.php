<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Hóa Đơn</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Danh sách hóa đơn</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>ID hóa đơn</th>
                                    <th>Thông tin khách hàng</th>
                                    <th>Thông tin liên lạc</th>
                                    <th>Tổng tiền hóa đơn</th>
                                    <th>Ngày lập hóa đơn</th>
                                    <th>Hình thức thanh toán</th>
                                    <th>Trạng thái</th>
                                    <th>Cập nhật</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count = 1;
                                foreach ($bill as $key => $value){
//                                    var_dump($value);
                                    ?>
                                <tr>
                                    <td><?php echo $count++ ?></td>
                                    <td><a href="billDetail.php?id=<?php echo $value->ID ?>">HD<?php echo $value->ID ?></a></td>
                                    <td>
                                        <ul>
                                            <li>Họ tên: <?php echo $value->ten_khach_hang ?></li>
                                            <li>Địa chỉ: <?php echo $value->dia_chi ?></li>
                                        </ul>
                                    </td>
                                    <td>Số điện thoại: <?php echo $value->so_dien_thoai ?></td>
                                    <td><?php echo number_format($value->tri_gia, 0,',','.') ?> VNĐ</td>
                                    <td><?php echo date("H:i:s d-m-Y", strtotime($value->ngay_lap))?></td>
                                    <td><?php echo $value->hinh_thuc_thanh_toan ?></td>
                                    <td style="font-size: 15px" class="badge badge-pill <?= ($value->trang_thai == 1)?'badge-success':'badge-warning' ?>" ><?php echo ($value->trang_thai == 1)?'Đã thanh toán':'Chưa thanh toán' ?></td>
                                    <td>
                                        <a class="btn btn-primary" href="updateBill.php?id=<?php echo $value->ID ;?>"><i class="m-r-10 mdi mdi-autorenew"></i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>


