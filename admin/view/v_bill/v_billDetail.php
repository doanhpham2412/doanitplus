<script>


    $("#updateBill").show();
    $("#printBill").show();
</script>

<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Hóa Đơn</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body" id="card-body">
                        <h5 class="card-title">Chi Tiết Hóa Đơn ( HD<?= $id ?> ) </h5>
                        <div class="card" style="width: 50rem;">
                            <table class="table">
                                <?php foreach ($bill_detail as $key => $value){ ?>
                                <tr>
                                    <th scope="row">Tên khách hàng</th>
                                    <td><?php echo $value->ten_khach_hang ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Số điện thoại</th>
                                    <td><?php echo "0".$value->so_dien_thoai ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Địa chỉ</th>
                                    <td><?php echo $value->dia_chi ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Hình thức thanh toán</th>
                                    <td><?php echo $value->hinh_thuc_thanh_toan ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Tổng số tiền</th>
                                    <td><strong><?php echo number_format($value->tri_gia, 0,',','.') ?> </strong>VNĐ</td>
                                </tr>
                                <tr>
                                    <th scope="row">Ghi chú</th>
                                    <td><input class="form-control" id="exampleFormControlTextarea1" rows="3" value="<?php echo $value->ghi_chu ?>"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Trạng Thái</th>
                                    <td><?php echo ($value->trang_thai == 1)?'Đã thanh toán':'Chưa thanh toán' ?></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th scope="row"> <a class="btn btn-primary" id="updateBill" href="updateBill.php?id=<?php echo $value->ID ;?>">Cập nhật thanh toán</a></th>
                                    <th scope="row"> <button id="printBill"class="btn btn-success" onclick="PrintElemAdmin('card-body')">In hóa đơn</button></th>
                                </tr>
                                <?php
                                break;
                                }
                                ?>
                            </table>
                        </div>
                        <h5 class="card-title">Chi Tiết Sản Phẩm Hóa Đơn ( HD<?php echo $id ?> ) </h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>ID hóa đơn</th>
                                    <th>Tên mặt hàng</th>
                                    <th>Đơn giá</th>
                                    <th>Số lượng</th>
                                    <th>Thành Tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $count = 1;
                                foreach ($bill_detail as $key => $value){
//                                    print_r($bill_detail);

                                    ?>
                                    <tr>
                                        <td><?php echo $count++ ?></td>
                                        <td>HD<?php echo $value->ID ?></a></td>
                                        <td><?php echo $value->ten_san_pham ?></td>
                                        <td><?php echo number_format($value->don_gia, 0,',','.') ?>VNĐ</td>
                                        <td><?php echo $value->so_luong ?></td>
                                        <td><?php echo number_format($value->thanh_tien, 0,',','.') ?>VNĐ</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>


    <script>

        function PrintElemAdmin(elem)
        {

            $("#updateBill").hide();
            $("#printBill").hide();
            var mywindow = window.open('', 'PRINT', 'height=400,width=600');

            mywindow.document.write('<html><head>' +
                '<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha\n' +
                '    384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">' +
                '' +
                '' +
                '<title>' + "In hóa đơn"  + '</title>');
            mywindow.document.write('</head><body >');

            mywindow.document.write('<h1>' + "In hóa đơn"  + '</h1>');
            mywindow.document.write(document.getElementById(elem).innerHTML);
            mywindow.document.write('</body></html>');
            // mywindow.document.close(); // necessary for IE >= 10
            // mywindow.focus(); // necessary for IE >= 10*/
            // mywindow.print();
            // mywindow.close();
            return true;
        }
    </script>



