



<div class="page-wrapper">
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form  class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Cập nhật người dùng</h4>


                            <input type="hidden"  class="form-control" value="<?= $s_id?>" name = "id_user">

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên Khách Hàng</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control" value="<?= $s_fullName?>" name = "fullName" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên đăng nhập</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control" value="<?= $s_userName ?>" name = "userName" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input  class="form-control" value="<?= $s_email?>"  name = "email"readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Ngày đăng ký</label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control" value="<?= $s_dateRegister?>" name = "dateRegister" readonly>
                                    </div>
                                </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Ngày đăng nhập cuối</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" value="<?=  $s_dateLastLogin ?>" name = "dateLastLogin" readonly>
                                </div>
                            </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Trạng Thái</label>
                                    <div class="col-md-9">
                                        <select name="active" class="select2 form-control custom-select" style="width: 100%; height:36px;">
                                            <option value="0" <?= ($s_active == "" || $s_active == 0)?'selected':''?> >Không hoạt động</option>
                                            <option value="1" <?= $s_active == 1 ?'selected':''?>>Đang hoạt động</option>
                                        </select>
                                    </div>
                                </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Loại người dùng</label>
                                <div class="col-md-9">
                                    <select name="id_typeUser" class="select2 form-control custom-select" style="width: 100%; height:36px;">

                                        <?php  foreach ($typeUser as $value){

                                          ?>
                                            <option value="<?= $value->ID?>" <?=( $s_idTypeUser == $value->ID)?'selected':'' ?> ><?= $value->ten_loai_nguoi_dung?></option>
                                            <?php
                                        }    ?>

                                    </select>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" name = "submitForm" class="btn btn-primary">Cập Nhật</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
