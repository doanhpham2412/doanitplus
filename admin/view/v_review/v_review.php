<div class="page-wrapper">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <button type="button" class="btn btn-primary btn-sm" onclick="window.open('updateCoupon.php')"
                            value="">Thêm mới
                    </button>

                    <!--                   --><? //= var_dump($accountTypes)?>

                    <!---->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                </div>
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card-body">

                                <h5 class="card-title">Quản lý khuyến mãi</h5>

                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">

                                        <thead>
                                        <tr>

                                            <th>Người dùng</th>
                                            <th>Sản phẩm</th>
                                            <th>Đánh giá</th>
                                            <th>Ngày tạo</th>
                                            <th style="width: 15%">Thao tác</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            include_once "models/m_product.php";
                                            include_once "models/m_account.php";
                                            $m_product = new m_product();
                                            $m_account = new m_account();
                                            foreach ($list as $review) {
                                                $name_user = "";
                                                $user = $m_account->selectOne($review->id_nguoi_dung);
                                                if($user)
                                                {
                                                    $name_user = $user->ten_khach_hang;
                                                }
                                                $name_product  = "";
                                                $product = $m_product->selectOne($review->id_san_pham);
                                                if($product)
                                                {
                                                    $name_product = $product->ten_san_pham;
                                                }


                                                ?>
                                                <tr>

                                                    <input type="hidden" name="id_review"id="id_review" value="<?=$review->id?>">
                                                    <input type="hidden" name="status" value="<?= $review->trang_thai?>">
                                                    <td><?= $name_user ?></td>
                                                    <td><?=$name_product?></td>
                                                    <td><?=$review->noi_dung?></td>
                                                    <td><?=  date("d-m-Y",strtotime($review->ngay_tao)) ?></td>
                                                    <td>
                                                        <button onclick="changeStatus(<?=$review->id?>,<?=$review->trang_thai?>)" name = "submitReview" id="submitReview" class="btn btn-<?php if($review->trang_thai == 0 )
                                                        {
                                                            echo "danger";
                                                        }
                                                        else
                                                        {
                                                            echo "info";
                                                        }

                                                        ?>"><?php if($review->trang_thai == 0 )
                                                            {
                                                                echo "Bỏ kích hoạt";
                                                            }
                                                            else
                                                            {
                                                                echo "Kích hoạt";
                                                            }?>



                                                        </button>
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="deleteReview('<?= $review->id?>')">Xóa</button>

                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>

                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
<script>
    function changeStatus(id,status) {
        $.post('api/updateReview.php',{
            id : id,
            status : status
        },function (data) {
            swal("Success",data,"success").then(()=>{  location.reload();});


        })



    }

</script>