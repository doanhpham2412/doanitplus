<?php
include ("database.php");
class m_bill extends database {
    public function showBill(){
        $sql = "Select hd.*, kh.ten_khach_hang,kh.so_dien_thoai,kh.dia_chi
                from hoa_don as hd, khach_hang as kh 
                WHERE hd.ID_nguoi_dung  = kh.ID_nguoi_dung ";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function showDetailBill($id){
        $sql = "Select hd.*, kh.ten_khach_hang,sp.ten_san_pham,sp.don_gia,cthd.so_luong,cthd.thanh_tien, kh.ten_khach_hang,kh.so_dien_thoai,kh.dia_chi
                from hoa_don as hd, khach_hang as kh,san_pham as sp, ct_hoa_don as cthd 
                WHERE hd.ID_nguoi_dung  = kh.ID_nguoi_dung AND hd.ID = cthd.ID_hoa_don AND sp.ID = cthd.ID_san_pham AND hd.ID = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }

    public function updateBill($trang_thai,$id){
            $sql = "UPDATE hoa_don set trang_thai = ? where ID = ?";
            $this->setQuery($sql);
            return $this->execute(array($trang_thai,$id));

    }
}