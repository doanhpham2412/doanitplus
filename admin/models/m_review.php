<?php
require_once ("database.php");
class m_review extends database{

    public function selectAll(){
        $sql = "SELECT * FROM review ";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function updateReview($ID_review,$status)
    {
        $sql = "UPDATE review SET trang_thai = ? WHERE id = ?";
       $this->setQuery($sql);
        return $this->execute(array($status,$ID_review));
    }
    public function insertReview($ID_product,$ID_user, $comment,$status,$created_at)
    {
        $sql = "INSERT INTO review VALUES (?,?,?,?,?,?)";
        $this->setQuery($sql);
        $this->execute(array(null, $ID_product, $ID_user, $comment, $status, $created_at));
        return $this->getLastItem();
    }
    public function showReviews($id_product){
        $sql = "SELECT * FROM review WHERE id_san_pham = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_product));
    }
    public function showInfoUser($id_user)
    {
        $sql = "SELECT ten_dang_nhap,avatar FROM nguoi_dung where ID = ?";
        $this->setQuery($sql);
//        var_dump($this->loadRow(array($id_user)));
        return $this->loadRow(array($id_user));
    }



}