<?php
require_once ("database.php");
class m_user extends database{

    public function takeUserbyId($username ,$password){
        $sql = "SELECT * FROM nguoi_dung where ID_loai_nguoi_dung = ? AND ten_dang_nhap = ? AND mat_khau = ? OR email = ? AND mat_khau = ?";
        $this->setQuery($sql);
        return $this->loadRow(array(1,$username,md5($password),$username,md5($password)));

    }
    public function changePassRequire($email){
        $sql = "SELECT * FROM nguoi_dung where email = ? ";
        $this->setQuery($sql);
        return $this->loadRow(array($email));
    }
    public function changePass($email){
        $sql = "SELECT * FROM nguoi_dung where ten_dang_nhap = ? ";
        $this->setQuery($sql);
        return $this->loadRow(array($email));
    }
    public function updatePasswordbyID($id_user,$pass)
    {
        $sql = "UPDATE nguoi_dung SET  mat_khau = ? WHERE ID = ?";
        $this->setQuery($sql);
        return $this->loadRow(array(md5($pass),$id_user));

    }
    public function takeUserInfobyID($id_user){
        $sql = "SELECT * FROM nguoi_dung where ID = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id_user));
    }
    public function takeUserbyEmail($email){
        $sql = "SELECT * FROM nguoi_dung where email = ? AND active = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($email,1));
    }
    public function updatePasswordbyEmail($email,$pass)
    {
        $sql = "UPDATE nguoi_dung SET  mat_khau = ? WHERE email = ?";
        $this->setQuery($sql);
        return $this->loadRow(array(md5($pass),$email));

    }
    public function updateLastLoginDate($id)
    {
        $lastLoginDate = date("Y-m-d");
        $sql = "UPDATE nguoi_dung SET ngay_dang_nhap_cuoi = ? WHERE ID = ?";
        $this->setQuery($sql);
        $this->execute(array($lastLoginDate,$id));
    }






}



