<?php
require_once ("database.php");
class m_account extends database{
    public function selectAllType()
    {
        $sql = "Select * from loai_nguoi_dung ";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function selectAccountByID($id)
    {
        $sql = "Select nd.*,kh.ten_khach_hang from nguoi_dung as nd left join khach_hang as kh on nd.ID = kh.ID_nguoi_dung where nd.ID_loai_nguoi_dung = ?";

        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }

    public function selectAll(){
        $sql = "Select nd.*,kh.ten_khach_hang from nguoi_dung as nd left join khach_hang as kh on nd.ID = kh.ID_nguoi_dung";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function selectOne($id){
        $sql = "Select nd.*,kh.ten_khach_hang from nguoi_dung as nd left join khach_hang as kh on nd.ID = kh.ID_nguoi_dung where nd.ID = ?";
        $this->setQuery($sql);
        return $this->loadRow([$id]);
    }
    public function countUser(){
        $sql = "Select count(*) as sl from nguoi_dung";
        $this->setQuery($sql);
        return $this->loadRow();
    }
    public function update($id_typeUser,$active,$s_id)
    {
        $sql = "UPDATE nguoi_dung set ID_loai_nguoi_dung = ? , active = ? WHERE ID = ?";
        $this->setQuery($sql);
        return $this->execute(array($id_typeUser,$active,$s_id));

    }

}