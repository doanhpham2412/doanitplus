<?php
include_once 'check_login.php';
include_once("models/m_coupon.php");
include_once("models/m_product.php");

echo "<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>";
class c_coupon
{
    public function show()
    {
        $m_coupon = new m_coupon();
        $list = $m_coupon->selectAll();
        $title = "Khuyến mãi";
        $view = "view/v_coupon/v_coupon.php";
        include_once "templates/layouts.php";


    }
    public function update()
    {

        $s_couponName = $s_status = $s_dateEnd = $s_dateStart = $s_discount = $s_amount = "";
        $m_coupon = new m_coupon();
        $m_product = new m_product();
        $types = $m_product->selectAllType();

        if (isset($_GET['id'])) {
            $s_id = $_GET['id'];
            $resultSelect = $m_coupon->selectOne($s_id);

            if ($resultSelect) {
                $s_id = $resultSelect->ma_khuyen_mai;
                $s_couponName = $resultSelect->ten_khuyen_mai;
                $s_status = $resultSelect->trang_thai;
                $s_dateEnd = $resultSelect->ngay_ket_thuc;
                $s_amount = $resultSelect->so_luong;
                $s_dateStart = $resultSelect->ngay_bat_dau;
                $s_discount = $resultSelect->phan_tram_giam_gia;;

            } else {
                $s_id = "";
            }


        }
        if (isset($_POST['submit'])) {
            $id_type = getPOST('id_type');
            $couponName = getPOST('coupon_name');
            $status =   getPOST('status');
            $dateEnd = getPOST('date_end');
            $dateStart =  getPOST('date_start');
            $discount =  getPOST('discount');
            $amount = getPOST('amount');


            if (!empty($s_id)) {
                $id = getPOST('ID');
                $resultUpdate = $m_coupon->update($s_id,$id_type,$couponName,$discount,$dateStart,$dateEnd,$amount,$status);
                if ($resultUpdate) {
                    echo '<body><script>swal("Good job!", "Cập nhật thành công!", "success").then(()=>{window.location="coupon.php"})</script></body>';
                } else {
                    echo '<body><script> swal("Good job!", "Cập nhật thất bại!", "error").then(()=>{window.location="coupon.php"})</script></body>';

                }

//                  header("Location:banner.php");
//                var_dump($_SESSION['message']);
            } else {
                $resultInsert = $m_coupon->insert($id_type,$couponName,$discount,$dateStart,$dateEnd,$amount);
                if ($resultInsert) {
                    echo '<body><script>swal("Good job!", "Thêm thành công!", "success").then(()=>{window.location="coupon.php"})</script></body>';
                } else {
                    echo '<body><script> swal("Good job!", "Thêm thất bại!", "error").then(()=>{window.location="coupon.php"})</script></body>';

                }
//                header("Location:banner.php");
            }

        }
        $title = "Cập nhât khuyến mãi";
        $view = "view/v_coupon/v_add_coupon.php";
        include_once "templates/layouts.php";

    }
    public function delete()
    {
        $m_coupon = new m_coupon();
        $deletebanner = $m_coupon->delete();

        if ($deletebanner) {
            echo "Xóa thành công";
        } else {
            echo "Không xóa được";
            return;
        }
    }
//Helper::Gui_mail();


}
