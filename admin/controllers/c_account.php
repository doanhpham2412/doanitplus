<?php
include_once 'check_login.php';
include_once("models/m_account.php");
echo "<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>";
class c_account
{
    public function show()
    {

        if (isset($_SESSION['message'])) {
            echo "<script type='text/javascript'> alert('" . $_SESSION['message'] . "'); </script>";
            //to not make the error message appear again after refresh:
            unset($_SESSION['message']);
        }
        $m_account = new m_account();
        $list = $m_account->selectAll();
        $accountTypes = $m_account->selectAllType();
        $id_loai = "";

        if (isset($_POST['btnTimKiem'])) {
            $id_loai = $_POST['accountType'];
            if (!empty($id_loai))
                $list = $m_account->selectAccountByID($id_loai);
        }


        $title = "Tài khoản";
        $view = "view/v_account/v_account.php";
        include_once "templates/layouts.php";


    }

    public function update()
    {
        $s_id = $s_idTypeUser = $s_fullName = $s_userName = $s_email =  $s_dateRegister = $s_dateLastLogin =  $s_active = "";

        $m_account = new m_account();
        if (isset($_GET['id'])) {
            $typeUser = $m_account->selectAllType();
            $s_id = $_GET['id'];
            $resultSelect = $m_account->selectOne($s_id);
            if ($resultSelect) {

                $s_id = $resultSelect->ID;
                $s_idTypeUser = $resultSelect->ID_loai_nguoi_dung;
                $s_fullName = $resultSelect->ten_khach_hang;
                $s_userName = $resultSelect->ten_dang_nhap;
                $s_email =  $resultSelect->email;
                $s_dateRegister = $resultSelect->ngay_dang_ky;
                $s_dateLastLogin =  $resultSelect->ngay_dang_nhap_cuoi;
                $s_active = $resultSelect->active;


            }

        }
        else {
            header("Location:account.php");
            die();
        }
        if (isset($_POST['submitForm'])) {
                $id_user = getPOST('id_user');
                $id_TypeUser = getPOST('id_typeUser');
                $active = getPOST('active');
                $resultUpdate = $m_account->update($id_TypeUser,$active,$s_id);
                    if ($resultUpdate) {
                        $_SESSION['success'] = "";
                        echo "<body><script>swal('Success','Cập nhật thành công','success')</script></body>";
                    } else {
                        $_SESSION['error'] = "Cập nhật không thành công";
                        echo "<body><script>swal('Failed','Cập nhật không thành công','error')</script></body>";

                    }

        }
        $title = "Cập nhật người dùng";
        $view = "view/v_account/v_updateAccount.php";
        include_once "templates/layouts.php";

    }

    public function delete()
    {
        $m_banner = new m_account();
        $deletebanner = $m_banner->delete();

        if ($deletebanner) {
            echo "Xóa thành công";
        } else {
            echo "Không xóa được";
            return;
        }
    }


}
