<?php
session_start();
include_once "models/m_user.php";

echo "<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>";
class c_user{
    public function check_login(){
     if(isset($_POST['login'])) {
         if (isset($_POST['Username'])) {
             $user = $_POST['Username'];
         }
         if (isset($_POST['Password'])) {
             $pass = $_POST['Password'];
         }

         $this->saveLoginSession($user, $pass);
         if (isset($_SESSION['user_admin'])) {
             $user = $_SESSION['user_admin'];
             if($user->active == 0)
             {
                 unset($_SESSION['user_admin']);
                 $_SESSION['error'] = "Tài khoản đã bị khóa vui lòng liên hệ admin để cấp quyền truy cập";
                 echo "<script>window.location = 'login.php'</script>";
                 die();
             }

             $_SESSION['success'] = "Đăng nhập thành công";


             $ID = $user->ID;
             $m_user = new m_user();
             $m_user-> updateLastLoginDate($ID);
             header('Location: index.php');
             die();

         } else {
             $_SESSION['error'] = "Sai thông tin đăng nhập";
             echo "<script>window.location = 'login.php'</script>";
         }
     }
     else{
         header('Location:login.php');
         die();
     }

    }
    public function saveLoginSession($user,$pass){
        $m_user = new m_user();
        $user = $m_user->takeUserbyId($user,$pass);
        if(!empty($user)) {
            $_SESSION['user_admin'] = $user;
        }

    }
    public function logout(){
        unset($_SESSION['user_admin']);
        echo '<body><script>swal("Good job!", "Đăng xuất thành công", "success");</body></script>';
        echo "<script>window.location = 'login.php';</script>";
    }
    public function changePass(){
        if (isset($_POST['submit'])) {
            $m_user = new m_user();
            $user_admin = $_SESSION['user_admin'];
            $id_user = $user_admin->ID;
            $pass = getPOST('pass');
            $newpass = getPOST('newpass');
            $renewpass = getPOST('renewpass');

            if(md5($pass) == $user_admin->mat_khau)
            {
                $m_user->updatePasswordbyID($id_user,$newpass);
                echo '<body><script>swal("Thành công!", "Đổi mật khẩu thành công", "success").then(()=>{window.location = "index.php"});</script></body>';
                $_SESSION['user_admin']->mat_khau = md5($newpass);
            }
            else{
                echo '<body><script>swal("Failed", "Mật khẩu không đúng", "warning");</script></body>';

            }

        }
        $title = "Đổi mật khẩu";
        $view = "view/v_change_pass.php";
        include_once "templates/layouts.php";
    }
    public function  forgot_pass()
    {
            include_once "../libs/Helper.php";
            $email = getPOST("email");
            $m_user = new m_user();
            $user = $m_user->takeUserbyEmail($email);
            if(empty($user))
            {
                $_SESSION['error'] = "Email không hợp lệ";
                header("Location:login.php");
                return;
            }
            $random =   generateRandomString(8);
            $_SESSION['randomCode'] = $random;
            $title = "Yêu cầu thay đổi mật khẩu";
            $content = "<p>Chúng tôi nhận được yêu cầu đổi mật khẩu từ bạn ,nếu không phải bạn hãy bỏ qua tin nhắn này</p> ";
            $content .= "<p>Mật khẩu mới của bạn là </p> ".$random;
            $bcc = $email;
            Helper::send($title,$content,$bcc);
            $m_user->updatePasswordbyEmail($email,$random);
            echo '<body><script>swal("Thành công!", "Gửi thành công !Vui lòng xác nhạn trong email", "success").then(()=>{window.location = "login.php"});</script></body>';
//        header('Location:login.php');
//        die();
    }
}
