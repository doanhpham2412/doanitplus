<?php
include_once 'check_login.php';
include ("models/m_bill.php");
class c_bill{
    public function showBill(){

        $m_bill = new m_bill();
        $bill = $m_bill->showBill();

        $title = "Đơn hàng";
        $view = "view/v_bill/v_bill.php";
        include_once "templates/layouts.php";
    }
    public function showDetailBill($id){
        if(isset($_GET['id'])){
            $id = $_GET['id'];
        $m_bill = new m_bill();
        $bill_detail = $m_bill->showDetailBill($id);

        $title = "Chi tiết hóa đơn";
        $view = "view/v_bill/v_billDetail.php";
        include_once "templates/layouts.php";
        }
    }
    public function updateBill(){
        $m_bill = new m_bill();
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $bill = $m_bill->showDetailBill($id);
            $title = "Cập nhật hóa đơn";
            $view = "view/v_bill/v_updateBill.php";
            include_once "templates/layouts.php";
        }
        if(isset($_POST['submit'])){
            $trang_thai = $_POST['trang_thai'];
            $id = $_POST['id_hoa_don'];
            $update = $m_bill->updateBill($trang_thai,$id);
//
            echo "<script>window.location = 'bill.php'</script>";
        }
    }
}