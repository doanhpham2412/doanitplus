function submitCoupon() {
    var amount = $("#amount").val();
    var discount = $("#discount").val();
    var coupon_name =  $("#coupon_name").val();

    var dateStartVal = $("#date_start").val();
    var dateEndVal = $("#date_end").val();

    var currentDate = new Date();
    var dateStart = new Date($("#date_start").val());
    var dateEnd = new Date($("#date_end").val());


    if(coupon_name == "")
    {
        swal("Cảnh báo","Chưa nhập tên khuyến mãi","warning");
        return false;
    }

    if(discount < 0 || discount > 90 || discount == "" )
    {
        swal("Cảnh báo","Phần trăm giảm giá không hợp lệ(từ 0 % -> 90%)","warning");
        return false;
    }
    if(dateStart.getTime() <= currentDate.getTime() || dateStartVal == ""  )
    {
        swal("Cảnh báo","Ngày bắt đầu không hợp lệ","warning");
        return false;

    }
    if(dateEnd.getTime() <= dateStart.getTime() || dateEndVal == "")
    {
        swal("Cảnh báo","Ngày hết hạn không hợp lệ","warning");
        return false;

    }
    if(amount < 0 || amount == "")
    {
        swal("Cảnh báo","Số lượng nhỏ hơn 0","warning");
        return false;
    }
}
function submitProduct() {
    var name = $("#name").val();
    var price = $("#price").val();
    var desc = $("#desc").val();
    var amount = $("#amount").val();
    if( name == "")
    {
        swal("Cảnh báo","Chưa nhập tên sp","warning");
        return false;
    }
    if(price < 0 || price == "")
    {
        swal("Cảnh báo","Giá sản phẩm không hợp lệ","warning");
        return false;
    }
    if( desc == "")
    {
        swal("Cảnh báo","Chưa nhập mô tả sản phẩm","warning");
        return false;
    }
    if(amount < 0 || amount == "" || amount > 999)
    {
        swal("Cảnh báo","Số lượng không hợp lệ","warning");
        return false;
    }

}
