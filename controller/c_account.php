<?php
echo "<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>";
session_start();
include "models/m_customer.php";
include "models/m_user.php";
include "models/m_bill.php";
class c_account
{
    public function  myAccount()
    {
        $click = getGET('click');
        $data = $_SESSION['user'];
        $id = $data->ID;
        $m_customer = new m_customer();
        $m_bill = new m_bill();
        $m_user = new m_user();
        $user = $_SESSION['user'];
        $name = $gender = $DoB = $address = $email = $phone = "";
        $customer = $m_customer->showCustomerById($id);
        $oldAvatar  = $user->avatar;
//        var_dump($_SESSION['user']);
        if(isset($_POST['changeAvatar']))
        {
            if(isset($_FILES['avatar']))
            {
                $avatar = $_FILES['avatar']['error'] == 0 ? $_FILES['avatar']['name']:"";
                if($avatar == "")
                {
                    echo '<script>swal("Chưa chọn avatar")</script>';
                }
                else{
                    if($oldAvatar != "default.jpg" && $oldAvatar != "")
                    {
                        unlink("public/image/avatar/".$oldAvatar);
                    }
                    move_uploaded_file($_FILES['avatar']['tmp_name'],"public/image/avatar/".$avatar);
                    $m_user->changeAvatar($avatar,$user->ID);
                    echo '<script>swal("Đổi avatar thành công")</script>';
                    $_SESSION['user']->avatar  = $avatar;
                }
            }

        }


        if($customer)
        {
            $name = $customer->ten_khach_hang;
            $gender = $customer->gioi_tinh;
            $DoB = $customer->ngay_sinh;
            $address = $customer->dia_chi;
            $email = $customer->email;
            $phone = $customer->so_dien_thoai;
        }


        if(isset($_POST['submitProfile']))
        {
            $name = getPOST("name");
            $gender = getPOST("gender");
            $DoB = getPOST("DoB");
            $address = getPOST("address");
            $email = getPOST("email");
            $phone = getPOST("phone");
            $pass =  getPOST("password");
            if(!$customer)
            {
                $customer_update = $m_customer->insertCustomer($user->ID,$name,$gender,$DoB,$address,$phone,$email);
            }
            else{
                    $customer_update = $m_customer->updateCustomer($user->ID,$name,$gender,$DoB,$address,$phone,$email);
            }

        }

//        var_dump($_POST);
        $bills = $m_bill->showBillbyId($user->ID);





        $title = "Quản lý tài khoản";
        $view = "views/account/v_profile.php";
        include_once "templates/layouts.php";


    }

}
