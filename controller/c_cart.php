<?php
echo "<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>";
session_start();
include_once "models/m_product.php";
class c_cart{

    public function showCart(){

        if(isset($_SESSION['cartList'])){
            $cartList = $_SESSION['cartList'];
            $title = "Giỏ hàng";
            $view = "views/cart/v_cart.php";
            require_once "templates/layouts.php";
        }
        else{
            echo "<body><script>
               swal('Giỏ hàng trống','Bạn sẽ trở về trang chủ','warning').then(()=>{window.location = 'index.php';});
            </script></body>";
        }

    }
}