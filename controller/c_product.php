<?php
echo "<script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>";
session_start();
include_once "models/m_product.php";
include_once "models/m_review.php";


class c_product{

    public function showProductDetail()
    {
        $id = $id_type = $ten_san_pham = $don_gia = $mo_ta = $so_luong = $hinh_san_pham = $luot_xem = $loai = "";
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $m_product = new m_product();
            $m_review = new m_review();
            $product = $m_product->showProduct($id);
            if ($product) {
                $id_product = $product->ID;
                $id_type = $product->ID_loai_san_pham;
                $ten_san_pham = $product->ten_san_pham;
                $don_gia = $product->don_gia;
                $mo_ta = $product->mo_ta;
                $details = $product->chi_tiet_san_pham;
                $so_luong = $product->so_luong;
                $hinh = $product->hinh_san_pham;
                $luot_xem = $product->luot_xem;
                $type = $product->ID_loai_san_pham;
                $typeName = $m_product->showType($id_type)->ten_loai_san_pham;
                $related_products = $m_product->showProductbyType($id_type);
                $reviews = $m_review->showReviews($id_product);
            } else {
                $_SESSION['error'] = "Không tồn tại sản phẩm này";
                header("Location:shop.php");
            }

            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $comment = getPOST('review_comment');
                if ($comment == "") {
                    $_SESSION['error'] = "Chưa nhập đánh giá";
                } else {
                    if (isset($_SESSION['user'])) {
                        $user = $_SESSION['user'];
                        $dateC = date("Y-m-d");
                        $insertR = $m_review->insertReview($id_product, $user->ID, $comment, 1, $dateC);
                        if ($insertR) {
                            $_SESSION['success'] = "Thêm đánh giá thành công";
                            header("Location:product_details.php?id=" . $id_product);
                            die();
                        } else {
                            $_SESSION['error'] = "Thêm không thành công";
                        }
                    } else {
                        $_SESSION['error'] = "Bạn chưa đăng nhập tài khoản vui lòng đăng nhập và mua hàng để đánh giá sản phẩm";
//                        var_dump($_SESSION);
                        echo "<script>window.location= 'product_details.php?id=$id_product'</script>";
                        die();

                    }
                }
            }
            $title = "Chi tiết sản phẩm";
            $view = "views/products/product_details.php";
            require_once "templates/layouts.php";
        } else {
            header("Location:shop.php");
        }
    }
}