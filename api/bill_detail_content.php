<?php
include "../models/m_bill.php";
include "../models/m_product.php";

$id_bill = $_GET['id_bill'];
$_GET['id_bill'] = "";
$m_bill = new m_bill();
$m_product = new m_product();
$bill = $m_bill->showBillbyId($id_bill);
$bill_detail = $m_bill->showBillDetailbyId($id_bill);
$total = 0 ;
foreach ($bill_detail as $bill) {

    $product = $m_product->showProduct($bill->ID_san_pham);
    $image = $product->hinh_san_pham;
    $productName = $product->ten_san_pham;
    $thanh_tien = $bill->thanh_tien;
    $total += $thanh_tien;
    ?>
    <tr>
        <td><a style="color: dodgerblue;" onclick=" window.open('product_details.php?id=<?=$bill->ID_san_pham?>')"><?= $productName ?></a></td>
        <td><a style="color: dodgerblue" onclick=" window.open('product_details.php?id=<?=$bill->ID_san_pham?>')"><img height="50px" width="50px" src="public/image/product/<?= $image ?>" alt=""></a></td>
        <td><?= $bill->so_luong ?></td>
        <td><span class="success"><?= number_format($product->don_gia,"0",",",".")  ?> VND</span></td>
        <td><span class="success"><?= number_format($thanh_tien,"0",",",".")  ?> VND</span></td>
<!--        <td><a onclick="window.print()" class="view">Print Bill</a></td>-->
    </tr>
    <?php
}
    echo '<tr >
<td style="text-align: left" colspan="4">Tổng</td>
<td><strong>'.number_format($total,"0",",",".") .' VND</strong></td>
</tr>';
?>