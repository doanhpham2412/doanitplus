
<?php
include_once "models/m_api.php";
include_once "models/m_product.php";

include_once "models/m_coupon.php";
include_once "models/m_user.php";
if(isset($_SESSION['user']))
{
    $user = $_SESSION['user'];
    $m_user= new m_user();
    $status = $m_user->checkStatus($user->ID);
    if($status == 0)
    {
        unset($_SESSION['user']);
        $_SESSION['error'] = "Tài khoản đã bị khóa vui lòng liên hệ admin để cấp quyền truy cập";
        echo "<script>window.location = 'login.php'</script>";
        die();
    }

}


$m_product = new m_product();
$m_coupon = new m_coupon();
$types  = $m_product->showAllTypes();
$tong = 0;
$totalqty = 0;
$m_api = new m_api();
$cart = [];
if(isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $cart_user = 'cart' . $user->ID;
    if (isset($_SESSION[$cart_user])) {
        $cartUser = $_SESSION[$cart_user];
        setcookie('cart', $cartUser, time() + 7 * 24 * 30 * 12, '/');
        $cart = json_decode($cartUser, true);
        $m_api->QueryFromCart($cart);
    }
}else{
    if(isset($_COOKIE['cart']) && $_COOKIE['cart'] !== "[]") {
        $json = $_COOKIE['cart'];
        $cart = json_decode($_COOKIE['cart'], true);
    }else{
        unset($_SESSION['cartList']);
    }
}

if(isset($_SESSION['cartList']))
{

    $cartList = $_SESSION['cartList'];
}

?>
<body>

<?php
if(isset($_SESSION['success']) )
{
    echo '<script>swal("Success","'.$_SESSION['success'].'","success")</script>';
    unset($_SESSION['success']);
}
if(isset($_SESSION['error']) )
{
    echo '<script>swal("Failed","'.$_SESSION['error'].'","error")</script>';
    unset($_SESSION['error']);

}


?>
<!--Offcanvas menu area start-->
<div class="off_canvars_overlay"></div>
<div class="mini_cart">
    <div class="cart_close">
        <div class="cart_text">
            <h3>Giỏ hàng</h3>
        </div>
        <div class="mini_cart_close">
            <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
        </div>
    </div>
    <?php
    if (isset($cartList)) {

        foreach ($cartList as $item) {
            for ($i = 0; $i < count($cart); $i++) {
                if ($item->ID == $cart[$i]['id']) {
                    $qty = $cart[$i]['num'];
                    $totalqty += $cart[$i]['num'];
                }
            }

            $coupon = $m_coupon->selectCouponbyProductType($item->ID_loai_san_pham);
            if (!$coupon) {
                $currentPrice = $item->don_gia;
            } else {
                $discount = $coupon->phan_tram_giam_gia;
                $currentPrice = $item->don_gia * (1 - $discount / 100);

            }
            $tong += $currentPrice * $qty;
            ?>
            <div class="cart_item">
                <div class="cart_img">
                    <a href="product_details.php?id=<?= $item->ID ?>"><img src="public/image/product/<?= $item->hinh_san_pham ?>" alt=""></a>
                </div>
                <div class="cart_info">
                    <a href="#"><?php $item->ten_san_pham ?></a>
                    <p>Qty: <?= number_format($qty, 0, ',', '.') ?> *
                        <span> <?= number_format($currentPrice, 0, ',', '.') ?> </span></p>
                </div>

                <div class="cart_remove">
                    <a href="#"><i class="ion-android-close" onclick="DeleteFromCart(<?= $item->ID ?>)"></i></a>
                </div>
            </div>

            <?php
        }
    }
    ?>

    <div class="mini_cart_table">
        <!--                        <div class="cart_total">-->
        <!--                            <span>Sub total:</span>-->
        <!--                            <span class="price">-->
        <!--</span>-->
        <!--                        </div>-->
        <div class="cart_total mt-10">
            <span>Tổng:</span>
            <span class="price"><?= number_format($tong, 0, ',', '.') ?></span>
        </div>
    </div>
    <div class="mini_cart_footer">
        <div class="cart_button">
            <a href="cart.php">Xem giỏ hàng</a>
        </div>
        <div class="cart_button">
            <a class="active" href="check_out.php">Thanh toán</a>
        </div>

    </div>
</div>
<div class="Offcanvas_menu">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="canvas_open">
                    <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                </div>
                <div class="Offcanvas_menu_wrapper">
                    <div class="canvas_close">
                        <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
                    </div>
                    <div class="antomi_message">
                        <p>Get free shipping – Free 30 day money back guarantee</p>
                    </div>
                    <div class="header_top_settings text-right">
                        <ul>
                            <li><a href="#">Store Locations</a></li>
                            <li><a href="#">Track Your Order</a></li>
                            <li>Hotline: <a href="tel:+0123456789">0705502529</a></li>
                            <li>Quality Guarantee Of Products</li>
                        </ul>

                    </div>
                                       <div class="search_container">-->
                                                                    <form action="#">
                                                                        <div class="hover_category">
                                                                            <select class="select_option" name="select" id="categori1">
                                                                                <option selected value="1">All Categories</option>


                                                                            </select>
                                                                        </div>
                                                                        <div class="search_box">
                                                                            <input placeholder="Search product..." type="text">
                                                                            <button type="submit">Search</button>
                                                                        </div>
                                                                    </form>
                                        </div>
                    <div id="menu" class="text-left ">
                        <ul class="offcanvas_main_menu">
                            <li class="menu-item-has-children active">
                                <a href="#">Trang chủ</a>
                                <ul class="sub-menu">
                                    <li><a href="index.html">Home 1</a></li>
                                    <li><a href="index-2.html">Home 2</a></li>
                                    <li><a href="index-3.html">Home 3</a></li>
                                    <li><a href="index-4.html">Home 4</a></li>
                                    <li><a href="index-5.html">Home 5</a></li>
                                    <li><a href="index-6.html">Home 6</a></li>
                                    <li><a href="index-7.html">Home 7</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Mua sắm</a>
                                <ul class="sub-menu">
                                    <li class="menu-item-has-children">
                                        <a href="#">Shop Layouts</a>
                                        <ul class="sub-menu">
                                            <li><a href="shop.html">shop</a></li>
                                            <li><a href="shop-fullwidth.html">Full Width</a></li>
                                            <li><a href="shop-fullwidth-list.html">Full Width list</a></li>
                                            <li><a href="shop-right-sidebar.html">Right Sidebar </a></li>
                                            <li><a href="shop-right-sidebar-list.html"> Right Sidebar list</a></li>
                                            <li><a href="shop-list.html">List View</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="#">other Pages</a>
                                        <ul class="sub-menu">
                                            <li><a href="cart.html">cart</a></li>
                                            <li><a href="wishlist.html">Wishlist</a></li>
                                            <li><a href="checkout.html">Checkout</a></li>
                                            <li><a href="my-account.html">my account</a></li>
                                            <li><a href="404.html">Error 404</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="#">Product Types</a>
                                        <ul class="sub-menu">
                                            <li><a href="product-details.html">product details</a></li>
                                            <li><a href="product-sidebar.html">product sidebar</a></li>
                                            <li><a href="product-grouped.html">product grouped</a></li>
                                            <li><a href="variable-product.html">product variable</a></li>
                                            <li><a href="product-countdown.html">product countdown</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="cart.php">Giỏ hàng</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">pages </a>
                                <ul class="sub-menu">
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="faq.html">Frequently Questions</a></li>
                                    <li><a href="privacy-policy.html">privacy policy</a></li>
                                    <li><a href="contact.html">contact</a></li>
                                    <li><a href="login.html">login</a></li>
                                    <li><a href="404.html">Error 404</a></li>
                                    <li><a href="compare.html">compare</a></li>
                                    <li><a href="coming-soon.html">coming soon</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="my-account.html">my account</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="about.html">About Us</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="contact.html"> Contact Us</a>
                            </li>
                        </ul>
                    </div>
                    <div class="Offcanvas_footer">
                        <span><a href="#"><i class="fa fa-envelope-o"></i> demo@example.com</a></span>
                        <ul>
                            <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="pinterest"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<header>
    <div class="main_header header_padding">
        <div class="container">
            <!--header top start-->
            <div class="header_top">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-5">
                        <div class="antomi_message">
                            <p>Get free shipping – Free 30 day money back guarantee</p>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="header_top_settings text-right">
                            <ul>
                                <li><a href="#">Store Locations</a></li>
                                <li><a href="#">Track Your Order</a></li>
                                <li>Hotline: <a href="tel:+0123456789">0123456789 </a></li>

                                <?php
                                if (!isset($_SESSION['user'])) {


                                    echo '<li><a href="login.php" style="color: blue">Đăng nhập </a></li>
                                          <li><a href="register.php" style="color: red">Đăng ký </a></li>';
                                } else {
                                    $user = $_SESSION['user'];

//                                    echo '<li></li>';

                                    echo ' 
                                        
                                        <a href="my_account.php"><img width ="50px" height = "50px"src="public/image/avatar/'.$user->avatar.'" alt="user"
                                             class="rounded-circle" width="31"></a>
                                        <li style="height: 10px" class="nav-item" id="dropdown_area">
        
                                    <a href="javascript:void(0)"
                                       class="nav-link dropdown-toggle"
                                       href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                        
                                             '.$user->ten_dang_nhap.'
                                             </a>
                                             
                                    <div  class="dropdown-menu dropdown-menu-right user-dd animated">
                                        <a class="dropdown-item" href="my_account.php"><i
                                                    class="ti-user m-r-5 m-l-5"></i>Thông tin cá nhân</a>
                                        <a class="dropdown-item" href="my_account.php?click=orders"><i
                                                    class="ti-wallet m-r-5 m-l-5"></i> Đơn hàng</a>
                                        <a class="dropdown-item" href="javascript:void(0)"><i
                                                    class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                        <a class="dropdown-item" href="change_pass.php"><i
                                                    class="ti-settings m-r-5 m-l-5"></i> Đổi mật khẩu</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="logout.php?func=exit"><i
                                                    class="fa fa-power-off m-r-5 m-l-5"></i> Đăng xuất</a>
                                        <div class="dropdown-divider"></div>
                                       
                                </li>';
                                }
                                ?>


                        </div>
                    </div>
                </div>
            </div>
            <!--header top start-->

            <!--header middel start-->
            <div class="header_middle sticky-header">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-md-3 col-4">
                        <div class="logo">
                            <a href="index.php"><img src="public/assets/img/logo/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12">
                        <div class="main_menu menu_position text-center">
                            <nav>
                                <ul>
                                    <li><a href="index.php">Trang chủ</a></li>
                                    <li ><a href="shop.php">Mua sắm</a>
                                    </li>
                                    <li ><a href="cart.php">Giỏ hàng</a>

                                    </li>
<!--                                    <li><a href="#">pages <i class="fa fa-angle-down"></i></a>-->
<!--                                        <ul class="sub_menu pages">-->
<!--                                            <li><a href="about.html">About Us</a></li>-->
<!--                                            <li><a href="faq.html">Frequently Questions</a></li>-->
<!--                                            <li><a href="privacy-policy.html">privacy policy</a></li>-->
<!--                                            <li><a href="contact.html">contact</a></li>-->
<!--                                            <li><a href="login.html">login</a></li>-->
<!--                                            <li><a href="404.html">Error 404</a></li>-->
<!--                                            <li><a href="compare.html">compare</a></li>-->
<!--                                            <li><a href="coming-soon.html">coming soon</a></li>-->
<!--                                        </ul>-->
<!--                                    </li>-->
                                    <li><a href="about.html">Chính sách</a></li>
                                    <li><a href="about.html">Giới thiệu</a></li>
                                    <li><a href="contact.html"> Liên hệ</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-7 col-6">
                        <div class="header_configure_area">
                            <!--                            <div class="header_wishlist">-->
                            <!--                                <a href="wishlist.html"><i class="ion-android-favorite-outline"></i>-->
                            <!--                                    <span class="wishlist_count">10</span>-->
                            <!--                                </a>-->
                            <!--                            </div>-->
                            <div class="mini_cart_wrapper">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-shopping-bag"></i>
                                    <!--                                    <span class="cart_price">1221312321321213<i class="ion-ios-arrow-down"></i></span>-->
                                    <!--                                    <span class="cart_count">12</span>-->
                                    <span class="cart_price" ><?= number_format($tong, 0, ',', '.') ?> <i
                                                class="ion-ios-arrow-down"></i></span>
                                    <span class="cart_count" id="totalqty"><?= number_format($totalqty, 0, ',', '.') ?></span>

                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--header middel end-->

            <!--mini cart-->


            <!--mini cart end-->

            <!--header bottom start-->
            <div class="header_bottom">
                <div class="row align-items-center">
                    <div class="column1 col-lg-3 col-md-6">
                        <div class="categories_menu <?= !isset($banner_main) ? "categories_three":""?>">
                            <div class="categories_title">
                                <h2 class="categori_toggle">TẤT CẢ CÁC LOẠI</h2>
                            </div>
                            <div class="categories_menu_toggle">
                                <ul>
                                    <?php
                                   foreach ($types as $key => $value) {
                                   echo '<li><a href="shop.php?type='. $value->ID .'"> ' . $value->ten_loai_san_pham . '</a></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="column2 col-lg-6 ">
                        <div class="search_container">
                            <form action="" method="get" id = "searchForm" onsubmit="false">
                                <div class="hover_category">
                                    <select class="select_option" name="type" id="type">
                                        <option selected value="">Tất cả sản phẩm</option>
                                        <?php
                                        foreach ($types as $key => $value) {
                                            $selected = $value->ID == getGET("type") ? "selected":"";

                                            echo '<option '.$selected.' value="' . $value->ID . '">' . $value->ten_loai_san_pham . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="search_box">
                                    <!--                                    <input type="hidden" name = "query_string" value="">-->
                                    <input placeholder="Tìm kiếm sản phẩm..." type="text" name="search"
                                           value="<?php if (isset($_GET['search'])) {
                                               echo $_GET['search'];
                                           } ?>">
                                    <button type="submit" id="submit_search" name="submit_search" onclick="getLink()">Tìm kiếm</button>


                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="column3 col-lg-3 col-md-6">
                        <div class="header_bigsale">
                            <a href="#">BIG SALE BLACK FRIDAY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header bottom end-->
    </div>
</header>