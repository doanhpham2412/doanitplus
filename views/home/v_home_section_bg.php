<div class="home_section_bg">

    <!--product area start-->
    <div class="product_area deals_product">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_header">
                        <div class="section_title">
                            <h2>Ưu đãi trong tháng</h2>

                        </div>
                        <div class="product_tab_btn">
                            <ul class="nav" role="tablist" id="nav-tab">
                                <li>
                                    <a class="active" data-toggle="tab" href="#Fashion" role="tab" aria-controls="Fashion" aria-selected="true">
                                       Máy tính
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Games" role="tab" aria-controls="Games" aria-selected="false">
                                        Điện thoại
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Speaker" role="tab" aria-controls="Speaker" aria-selected="false">
                                        Âm thanh
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Mobile" role="tab" aria-controls="Mobile" aria-selected="false">
                                        Các thiết bị khác
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="Fashion" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">

                        <?php

                        foreach ($computer as $com)
                        {
                            $strtotime = strtotime(date("Y-m-d"))+1*60*60*60;
                            $dateEnd = date("Y-m-d",$strtotime);
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $dateEnd = $coupon->ngay_ket_thuc;
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia *(1 - $discount/100);
                            }
                            echo ' <article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img img-scaledown" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product1.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").' VND </span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                        <div class="countdown_text">
                                            <p><span>Hurry Up!</span> Offers ends in: </p>
                                        </div>
                                        <div class="product_timing">
                                            <div data-countdown="'.date("Y/m/d",strtotime($dateEnd)).'"></div>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>

                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>

                    </div>
                </div>
                <div class="tab-pane fade" id="Games" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php
                        foreach ($phone as $com)
                        {
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia * (1 - $discount/100);
                            }
                            echo ' <article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product1.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").' VND </span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                        <div class="countdown_text">
                                            <p><span>Hurry Up!</span> Offers ends in: </p>
                                        </div>
                                        <div class="product_timing">
                                            <div data-countdown="2022/04/01"></div>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>

                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Speaker" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php
                        foreach ($musix as $com)
                        {
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia *(1 - $discount/100);
                            }
                            echo ' <article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product1.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").' VND</span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                        <div class="countdown_text">
                                            <p><span>Hurry Up!</span> Offers ends in: </p>
                                        </div>
                                        <div class="product_timing">
                                            <div data-countdown="2022/04/01"></div>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>

                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Mobile" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php
                        foreach ($orther as $com)
                        {
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia *(1 - $discount/100);
                            }
                            echo ' <article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product1.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").' VND</span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                        <div class="countdown_text">
                                            <p><span>Hurry Up!</span> Offers ends in: </p>
                                        </div>
                                        <div class="product_timing">
                                            <div data-countdown="2022/04/01"></div>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>

                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--product area end-->

    <!--banner area start-->
    <div class="banner_area mb-55">
        <div class="container">
            <div class="row">
                <?php
                foreach ($banner_thumb as $item) {
                    echo '<div class="col-lg-6 col-md-6">
                            <figure class="single_banner " >
                                <div class="banner_thumb" >
                                    <a href="shop.php" class="img-cover" style="height: 300px"><img  src="public/image/banner/'.$item->hinh.'" alt=""></a>
                                </div>
                            </figure>
                        </div>';
                }
                ?>

            </div>
        </div>
    </div>
    <!--banner area end-->

    <!--product area start-->
    <div class="product_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_header">
                        <div class="section_title">
                            <h2>Sản phẩm nổi bật</h2>

                        </div>
                        <div class="product_tab_btn">
                            <ul class="nav" role="tablist" id="nav-tab2">
                                <li>
                                    <a class="active" data-toggle="tab" href="#Computer" role="tab" aria-controls="Computer" aria-selected="true">
                                        Máy tính
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Networking" role="tab" aria-controls="Networking" aria-selected="false">
                                        Điện thoại
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Computer2" role="tab" aria-controls="Computer2" aria-selected="false">
                                        Âm thanh
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Audio" role="tab" aria-controls="Audio" aria-selected="false">
                                        Khác
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="Computer" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php

                        for($i=0;$i<count($computer1)/2;$i++)
                        {

                                echo '<div class="product_items">';
                                for($j = $i*2;$j <= $i*2+1; $j++)
                                {

                                    if(isset($computer1[$j])) {
                                    $coupon = $m_coupon->selectCouponbyProductType($computer1[$j]->ID_loai_san_pham);
                                    if(!$coupon)
                                    {
                                        $currentPrice = $computer1[$j]->don_gia;
                                    }else{
                                        $discount = $coupon->phan_tram_giam_gia;
                                        $currentPrice = $computer1[$j]->don_gia * (1 - $discount/100);
//                                    var_dump($currentPrice); ;
                                    }
                                    ?>
                                    <article class="single_product">
                                        <figure>
                                            <div class="product_thumb">
                                                <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$computer1[$j]->ID?>"><img src="public/image/product/<?=$computer1[$j]->hinh_san_pham?>" alt=""></a>
                                                <a class="secondary_img" href="product_details.php?id=<?=$computer1[$j]->ID?>"><img src="public/image/product/product6.jpg" alt=""></a>
                                                <div class="label_product">
                                                    <span class="label_sale">Sale</span>
                                                </div>
                                                <div class="action_links">
                                                    <ul>
                                                        <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                                        <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                        <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product_content">
                                                <div class="product_content_inner">
                                                    <h4 class="product_name"><a href="product_details.php?id=<?=$computer1[$j]->ID?>"><?=$computer1[$j]->ten_san_pham?></a></h4>
                                                    <div class="price_box">
                                                        <span class="old_price"><?= number_format($computer1[$j]->don_gia,"0",",",".")?> VND</span>
                                                        <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                                    </div>
                                                </div>
                                                <div class="add_to_cart">
                                                    <a onclick="AddToCart(<?= $computer1[$j]->ID?>)" title="Add to cart">Add to cart</a>
                                                </div>

                                            </div>
                                        </figure>
                                    </article>
                                    <?php
                                }
                                }
                                echo '</div>';


                        }

                        ?>

                    </div>
                </div>
                <div class="tab-pane fade" id="Networking" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php

                        for($i=0;$i<count($phone1)/2;$i++)
                        {
                            echo '<div class="product_items">';
                            for($j = $i*2;$j <= $i*2+1; $j++)
                            {
                                if(isset($phone1[$j])) {
                                $coupon = $m_coupon->selectCouponbyProductType($phone1[$j]->ID_loai_san_pham);
                                if(!$coupon)
                                {
                                    $currentPrice = $phone1[$j]->don_gia;
                                }else{
                                    $discount = $coupon->phan_tram_giam_gia;
                                    $currentPrice = $phone1[$j]->don_gia *(1 - $discount/100);
                                }
                                ?>
                                <article class="single_product">
                                    <figure>
                                        <div class="product_thumb">
                                            <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$phone1[$j]->ID?>"><img src="public/image/product/<?=$phone1[$j]->hinh_san_pham?>" alt=""></a>
                                            <a class="secondary_img" href="product_details.php?id="><img src="public/image/product/product6.jpg" alt=""></a>
                                            <div class="label_product">
                                                <span class="label_sale">Sale</span>
                                            </div>
                                            <div class="action_links">
                                                <ul>
                                                    <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                    <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product_content">
                                            <div class="product_content_inner">
                                                <h4 class="product_name"><a href="product_details.php?id=<?=$phone1[$j]->ID?>"><?=$phone1[$j]->ten_san_pham?></a></h4>
                                                <div class="price_box">
                                                    <span class="old_price"><?= number_format($phone1[$j]->don_gia,"0",",",".")?> VND</span>
                                                    <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                                </div>
                                            </div>
                                            <div class="add_to_cart">
                                                <a onclick="AddToCart(<?=$phone1[$j]->ID?>)" title="Add to cart">Add to cart</a>
                                            </div>

                                        </div>
                                    </figure>
                                </article>
                                <?php
                            }
                            }
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Computer2" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">

                        <?php
                        for($i=0;$i<count($musix1)/2;$i++)
                        {


                            echo '<div class="product_items">';
                            for($j = $i*2;$j <= $i*2+1; $j++)
                            {
                        if(isset($musix1[$j])) {
                                $coupon = $m_coupon->selectCouponbyProductType($musix1[$j]->ID_loai_san_pham);
                                if(!$coupon)
                                {
                                    $currentPrice = $musix1[$j]->don_gia;
                                }else{
                                    $discount = $coupon->phan_tram_giam_gia;
                                    $currentPrice = $musix1[$j]->don_gia *(1 - $discount/100);
                                }
                                ?>
                                <article class="single_product">
                                    <figure>
                                        <div class="product_thumb">
                                            <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$musix1[$j]->ID?>"><img src="public/image/product/<?=$musix1[$j]->hinh_san_pham?>" alt=""></a>
                                            <a class="secondary_img" href="product_details.php?id=<?=$musix1[$j]->ID?>"><img src="public/image/product/product6.jpg" alt=""></a>
                                            <div class="label_product">
                                                <span class="label_sale">Sale</span>
                                            </div>
                                            <div class="action_links">
                                                <ul>
                                                    <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                    <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product_content">
                                            <div class="product_content_inner">
                                                <h4 class="product_name"><a href="product_details.php?id=<?=$musix1[$j]->ID?>"><?=$musix1[$j]->ten_san_pham?></a></h4>
                                                <div class="price_box">
                                                    <span class="old_price"><?= number_format($musix1[$j]->don_gia,"0",",",".")?> VND</span>
                                                    <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                                </div>
                                            </div>
                                            <div class="add_to_cart">
                                                <a onclick="AddToCart(<?=$musix1[$j]->ID?>)" title="Add to cart">Add to cart</a>
                                            </div>

                                        </div>
                                    </figure>
                                </article>
                                <?php
                            }
                            }
                            echo '</div>';

                        }
                        ?>

                    </div>
                </div>
                <div class="tab-pane fade" id="Audio" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php
                        for($i=0;$i<count($orther1)/2;$i++)
                        {


                            echo '<div class="product_items">';
                            for($j = $i*2;$j <= $i*2+1; $j++)
                            {
                                if(isset($orther1[$j])) {
                                $coupon = $m_coupon->selectCouponbyProductType($orther1[$j]->ID_loai_san_pham);
                                if(!$coupon)
                                {
                                    $currentPrice = $orther1[$j]->don_gia;
                                }else{
                                    $discount = $coupon->phan_tram_giam_gia;
                                    $currentPrice = $orther1[$j]->don_gia *(1 - $discount/100);
                                }
                                ?>
                                <article class="single_product">
                                    <figure>
                                        <div class="product_thumb">
                                            <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$orther1[$j]->ID?>"><img src="public/image/product/<?=$orther1[$j]->hinh_san_pham?>" alt=""></a>
<!--                                            <a class="secondary_img" href="product_details.php?id="><img src="public/image/product/product6.jpg" alt=""></a>-->
                                            <div class="label_product">
                                                <span class="label_sale">Sale</span>
                                            </div>
                                            <div class="action_links">
                                                <ul>
                                                    <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                                    <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                                    <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product_content">
                                            <div class="product_content_inner">
                                                <h4 class="product_name"><a href="product_details.php?id=<?=$orther1[$j]->ID?>"><?=$orther1[$j]->ten_san_pham?></a></h4>
                                                <div class="price_box">
                                                    <span class="old_price"><?= number_format($orther1[$j]->don_gia,"0",",",".")?> VND</span>
                                                    <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                                </div>
                                            </div>
                                            <div class="add_to_cart">
                                                <a onclick="AddToCart(<?=$orther1[$j]->ID?>)" title="Add to cart">Add to cart</a>
                                            </div>

                                        </div>
                                    </figure>
                                </article>
                                <?php
                            }
                                }
                            echo '</div>';
                            }

                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--product area end-->

    <!--product area start-->
    <div class="small_product_area mb-55">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_header">
                        <div class="section_title">
                            <h2>Sản phẩm bán chạy</h2>

                        </div>
                        <div class="product_tab_btn">
                            <ul class="nav" role="tablist" id="nav-tab3">
                                <li>
                                    <a class="active" data-toggle="tab" href="#Fashion2" role="tab" aria-controls="Fashion2" aria-selected="true">
                                       Máy tính
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Games2" role="tab" aria-controls="Games2" aria-selected="false">
                                        Điện thoại
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Headphone2" role="tab" aria-controls="Headphone2" aria-selected="false">
                                        Âm thanh
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Mobile2" role="tab" aria-controls="Mobile2" aria-selected="false">
                                        Các thiết bị khác
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="Fashion2" role="tabpanel">
                    <div class="product_carousel small_p_container  small_product_column3 owl-carousel">
                        <?php
                        for($i=0;$i<count($computer2)/2;$i++)
                        {
                            echo '<div class="product_items">';
                            for($j = $i*2;$j <= $i*2+1; $j++)
                            {
                                if(isset($computer2[$j])) {
                                $coupon = $m_coupon->selectCouponbyProductType($computer2[$j]->ID_loai_san_pham);
                                if(!$coupon)
                                {
                                    $currentPrice = $computer2[$j]->don_gia;
                                }else{
                                    $discount = $coupon->phan_tram_giam_gia;
                                    $currentPrice = $computer2[$j]->don_gia * (1 - $discount/100);

                                }
                                ?>
                                <figure class="single_product">
                                    <div class="product_thumb">
                                        <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$computer2[$j]->ID?>"><img src="public/image/product/<?=$computer2[$j]->hinh_san_pham?>" alt=""></a>
                                        <a class="secondary_img" href="product_details.php?id="><img src="public/image/product/product2.jpg" alt=""></a>
                                    </div>
                                    <div class="product_content">
                                        <h4 class="product_name"><a href="product_details.php?id=<?=$computer2[$j]->ID?>"><?=$computer2[$j]->ten_san_pham?></a></h4>
                                        <div class="product_rating">
                                            <ul>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="price_box">
                                            <span class="old_price"><?= number_format($computer2[$j]->don_gia,"0",",",".")?> VND</span>
                                            <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                        </div>
                                        <div class="product_cart_button">
                                            <a onclick="AddToCart(<?=$computer2[$j]->ID?>)" title="Add to cart"><i class="fa fa-shopping-bag"></i></a>
                                        </div>

                                    </div>
                                </figure>
                                <?php
                            }
                            }
                            echo '</div>';

                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Games2" role="tabpanel">
                    <div class="product_carousel small_p_container  small_product_column3 owl-carousel">
                        <?php
                        for($i=0;$i<count($phone2)/2;$i++)
                        {
                            echo '<div class="product_items">';
                            for($j = $i*2;$j <= $i*2+1; $j++)
                            {
                                if(isset($phone2[$j])) {
                                $coupon = $m_coupon->selectCouponbyProductType($phone2[$j]->ID_loai_san_pham);
                                if(!$coupon)
                                {
                                    $currentPrice = $phone2[$j]->don_gia;
                                }else{
                                    $discount = $coupon->phan_tram_giam_gia;
                                    $currentPrice = $phone2[$j]->don_gia * (1 - $discount/100);

                                }
                                ?>
                                <figure class="single_product">
                                    <div class="product_thumb">
                                        <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$phone2[$j]->ID?>"><img src="public/image/product/<?=$phone2[$j]->hinh_san_pham?>" alt=""></a>
                                        <a class="secondary_img" href="product_details.php?id=<?=$phone2[$j]->ID?>"><img src="public/image/product/product2.jpg" alt=""></a>
                                    </div>
                                    <div class="product_content">
                                        <h4 class="product_name"><a href="product_details.php?id=<?=$phone2[$j]->ID?>"><?=$phone2[$j]->ten_san_pham?></a></h4>
                                        <div class="product_rating">
                                            <ul>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="price_box">
                                            <span class="old_price"><?= number_format($phone2[$j]->don_gia,"0",",",".")?> VND</span>
                                            <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                        </div>
                                        <div class="product_cart_button">
                                            <a onclick="AddToCart(<?=$phone2[$j]->ID?>)" title="Add to cart"><i class="fa fa-shopping-bag"></i></a>
                                        </div>

                                    </div>
                                </figure>
                                <?php
                            }
                                }
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Headphone2" role="tabpanel">
                    <div class="product_carousel small_p_container  small_product_column3 owl-carousel">
                        <?php
                        for($i=0;$i<count($musix2)/2;$i++)
                        {
                            echo '<div class="product_items">';
                            for($j = $i*2;$j <= $i*2+1; $j++)
                            {
                        if(isset($musix2[$j])) {
                                $coupon = $m_coupon->selectCouponbyProductType($musix2[$j]->ID_loai_san_pham);
//                                var_dump($coupon);
                                if(!$coupon)
                                {
                                    $currentPrice = $musix2[$j]->don_gia;
                                }else{
                                    $discount = $coupon->phan_tram_giam_gia;
                                    $currentPrice = $musix2[$j]->don_gia * (1 - $discount/100);
                                }
                                ?>
                                <figure class="single_product">
                                    <div class="product_thumb">
                                        <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$musix2[$j]->ID?>"><img src="public/image/product/<?=$musix2[$j]->hinh_san_pham?>" alt=""></a>
                                        <a class="secondary_img" href="product_details.php?id=<?=$musix2[$j]->ID?>"><img src="public/image/product/product2.jpg" alt=""></a>
                                    </div>
                                    <div class="product_content">
                                        <h4 class="product_name"><a href="product_details.php?id=<?=$musix2[$j]->ID?>"><?=$musix2[$j]->ten_san_pham?></a></h4>
                                        <div class="product_rating">
                                            <ul>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="price_box">
                                            <span class="old_price"><?= number_format($musix2[$j]->don_gia,"0",",",".")?> VND</span>
                                            <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                        </div>
                                        <div class="product_cart_button">
                                            <a onclick="AddToCart(<?=$musix2[$j]->ID?>)" title="Add to cart"><i class="fa fa-shopping-bag"></i></a>
                                        </div>

                                    </div>
                                </figure>
                                <?php
                                    }
                                }
                            echo '</div>';
                        }


                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Mobile2" role="tabpanel">
                    <div class="product_carousel small_p_container  small_product_column3 owl-carousel">
                        <?php
                        for($i=0;$i<count($orther)/2;$i++)
                        {
                            echo '<div class="product_items">';
                            for($j = $i*2;$j <= $i*2+1; $j++)
                            {
                        if(isset($orther[$j])) {
                                $coupon = $m_coupon->selectCouponbyProductType($orther[$j]->ID_loai_san_pham);
//                                var_dump($coupon);
                                if(!$coupon)
                                {
                                    $currentPrice = $orther[$j]->don_gia;
                                }else{
                                    $discount = $coupon->phan_tram_giam_gia;
                                    $currentPrice = $orther[$j]->don_gia * (1 - $discount/100);
                                }
                                ?>
                                <figure class="single_product">
                                    <div class="product_thumb">
                                        <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id=<?=$orther[$j]->ID?>"><img src="public/image/product/<?=$orther[$j]->hinh_san_pham?>" alt=""></a>
                                        <a class="secondary_img" href="product_details.php?id="><img src="public/image/product/product2.jpg" alt=""></a>
                                    </div>
                                    <div class="product_content">
                                        <h4 class="product_name"><a href="product_details.php?id=<?=$orther[$j]->ID?>"><?=$orther[$j]->ten_san_pham?></a></h4>
                                        <div class="product_rating">
                                            <ul>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                                <li><a href="#"><i class="ion-android-star-outline"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="price_box">
                                            <span class="old_price"><?= number_format($orther[$j]->don_gia,"0",",",".")?> VND</span>
                                            <span class="current_price"><?= number_format($currentPrice,"0",",",".")?> VND</span>
                                        </div>
                                        <div class="product_cart_button">
                                            <a onclick="AddToCart(<?=$orther[$j]->ID?>)" title="Add to cart"><i class="fa fa-shopping-bag"></i></a>
                                        </div>

                                    </div>
                                </figure>
                                <?php
                                }
                            }
                            echo '</div>';
                        }
                        ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--product area end-->

    <!--product area start-->
    <div class="product_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_header">
                        <div class="section_title">
                            <h2>Hàng mới nhập</h2>

                        </div>
                        <div class="product_tab_btn">
                            <ul class="nav" role="tablist" id="nav-tab4">
                                <li>
                                    <a class="active" data-toggle="tab" href="#Computer3" role="tab" aria-controls="Computer3" aria-selected="true">
                                        Máy tính
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Networking2" role="tab" aria-controls="Networking2" aria-selected="false">
                                        Điện thoại
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Networking3" role="tab" aria-controls="Networking3" aria-selected="false">
                                        Ti vi
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#Audio2" role="tab" aria-controls="Audio2" aria-selected="false">
                                       Khác
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="Computer3" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">

                        <?php
                        foreach ($computer3 as $com)
                        {
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia *(1 - $discount/100);
                            }
                            echo '<article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product6.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").' VND</span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>
                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>

                    </div>
                </div>
                <div class="tab-pane fade" id="Networking2" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php
                        foreach ($phone3 as $com)
                        {
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia *(1 - $discount/100);
                            }
                            echo '<article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product6.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").' VND</span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>
                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Networking3" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php
                        foreach ($musix3 as $com)
                        {
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia *(1 - $discount/100);
                            }
                            echo '<article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product6.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").'VND</span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>
                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="Audio2" role="tabpanel">
                    <div class="product_carousel product_style product_column5 owl-carousel">
                        <?php
                        foreach ($orther3 as $com)
                        {
                            $coupon = $m_coupon->selectCouponbyProductType($com->ID_loai_san_pham);
                            if(!$coupon)
                            {
                                $currentPrice = $com->don_gia;
                            }else{
                                $discount = $coupon->phan_tram_giam_gia;
                                $currentPrice = $com->don_gia *(1 - $discount/100);
                            }
                            echo '<article class="single_product">
                            <figure>

                                <div class="product_thumb">
                                    <a class="primary_img img-scaledown" style="height: 200px" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/'.$com->hinh_san_pham.'" alt=""></a>
                                    <a class="secondary_img" href="product_details.php?id='.$com->ID.'"><img src="public/image/product/product6.jpg" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="wishlist"><a href="wishlist.html" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-tippy="Add to Wishlist"><i class="ion-android-favorite-outline"></i></a></li>
                                            <li class="compare"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-tippy="Add to Compare"><i class="ion-ios-settings-strong"></i></a></li>
                                            <li class="quick_button"><a href="#" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"  data-bs-toggle="modal" data-bs-target="#modal_box" data-tippy="quick view"><i class="ion-ios-search-strong"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content">
                                    <div class="product_content_inner">
                                        <h4 class="product_name"><a href="product_details.php?id='.$com->ID.'">'.$com->ten_san_pham.'</a></h4>
                                        <div class="price_box">
                                            <span class="old_price">'.number_format($com->don_gia,"0",",",".").' VND</span>
                                            <span class="current_price">'.number_format($currentPrice,"0",",",".").' VND </span>
                                        </div>
                                    </div>
                                    <div class="add_to_cart">
                                        <a onclick="AddToCart('.$com->ID.')" title="Add to cart">Add to cart</a>
                                    </div>

                                </div>
                            </figure>
                        </article>';
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--product area end-->

    <!--banner area start-->
    <div class="banner_area banner_style2 mb-55">
        <div class="container">
            <div class="row">
                    <?php
                    $arr = [];

                    foreach ($banner_thumb1 as $item) {
                      $arr[] = $item->hinh;
                    }
                    ?>

                <div class="col-lg-3 col-md-3">
                    <figure class="single_banner">
                        <div class="banner_thumb">
                            <a href="shop.html" style="height: 200px" class="img-cover"><img height="152px" src="public/image/banner/<?= $arr[0]  ?>" alt=""></a>
                        </div>
                    </figure>
                </div>
                <div class="col-lg-6 col-md-6">
                    <figure class="single_banner">
                        <div class="banner_thumb">
                            <a href="shop.html" style="height: 200px" class="img-cover"><img height="152px" src="public/image/banner/<?= $arr[1]  ?>" alt=""></a>
                        </div>
                    </figure>
                </div>
                <div class="col-lg-3 col-md-3">
                    <figure class="single_banner">
                        <div class="banner_thumb">
                            <a href="shop.html" style="height: 200px" class="img-cover"><img height="152px" src="public/image/banner/<?= $arr[2] ?>" alt=""></a>
                        </div>
                    </figure>
                </div>
                    <?php
                    ?>

            </div>
        </div>
    </div>
    <!--banner area end-->

</div>