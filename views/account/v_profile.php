<div class="account_page_bg">
    <div class="container">
        <section class="main_content_area">
            <div class="account_dashboard">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <!-- Nav tabs -->
                        <div class="dashboard_tab_button">
                            <ul role="tablist" class="nav flex-column dashboard-list" id="nav-tab">
<!--                                <li><a href="#dashboard" data-toggle="tab" class="nav-link active">Tổng quan</a></li>-->
<!--                                <li><a href="#downloads" data-toggle="tab" class="nav-link">Downloads</a></li>-->
                                <li><a id ="buttonAD" href="#account-details" data-toggle="tab" class="nav-link <?=$click==''?'active':"" ?>">Thông tin khách hàng</a></li>
                                <li> <a href="#orders" data-toggle="tab" class="nav-link <?=$click=='orders'?'active':"" ?>">Đơn hàng</a></li>
                                <li><a style="color: white" onclick="window.location ='change_pass.php';" data-toggle="tab" class="nav-link">Đổi mật khẩu</a></li>
                                <li><a style="color: white" onclick="function logout() {
                                }{window.location = 'logout.php?func=exit'}" class="nav-link">Đăng xuất</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard_content">
<!--                            <div class="tab-pane fade " id="dashboard">-->
<!--                                <h3>Dashboard </h3>-->
<!--                                <p>From your account dashboard. you can easily check &amp; view your <a href="#">recent orders</a>, manage your <a href="#">shipping and billing addresses</a> and <a href="#">Edit your password and account details.</a></p>-->
<!--                            </div>-->
                            <div class="tab-pane fade <?=$click=='orders'?'show active':"" ?>" id="orders">
                                <h3>Đơn hàng</h3>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Chi tiết hóa đơn</th>
                                            <th>Ngày đặt</th>
                                            <th>Tổng tiền</th>
                                            <th>Trạng thái</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <input type="hidden" id="id_bill">
                                        <?php
                                        $count =1;
                                        foreach ($bills as $bill)
                                        {
                                            ?>
                                            <tr>
                                                <td><?=$count++?></td>
                                                <td>  <a style="color: red" onclick="setValue('<?= $bill->ID ?>')">View</a></td>
                                                <td><?=date("d-m-Y",strtotime( $bill->ngay_lap)) ?></td>
                                                <td><?= number_format($bill->tri_gia,"0",",",".")  ?> VND</td>
                                                <td><span class="badge  <?= $bill->trang_thai == 1 ?"badge-success" :"badge-warning" ?>"><?= $bill->trang_thai == 1 ?"Đã thanh toán" :"Chưa thanh toán" ?></span></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="downloads">
                                <h3>Downloads</h3>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Downloads</th>
                                            <th>Expires</th>
                                            <th>Download</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Shopnovilla - Free Real Estate PSD Template</td>
                                            <td>May 10, 2018</td>
                                            <td><span class="danger">Expired</span></td>
                                            <td><a href="#" class="view">Click Here To Download Your File</a></td>
                                        </tr>
                                        <tr>
                                            <td>Organic - ecommerce html template</td>
                                            <td>Sep 11, 2018</td>
                                            <td>Never</td>
                                            <td><a href="#" class="view">Click Here To Download Your File</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="address">
                                <p>The following addresses will be used on the checkout page by default.</p>
                                <h4 class="billing-address">Billing address</h4>
                                <a href="#" class="view">Edit</a>
                                <p><strong>Bobby Jackson</strong></p>
                                <address>
                                    <span><strong>City:</strong> Seattle</span>,
                                    <br>
                                    <span><strong>State:</strong> Washington(WA)</span>,
                                    <br>
                                    <span><strong>ZIP:</strong> 98101</span>,
                                    <br>
                                    <span><strong>Country:</strong> USA</span>
                                </address>
                            </div>
                            <div class="tab-pane fade <?=$click==''?'show active':"" ?>" id="account-details"  >
                                <div><h3>Thông tin khách hàng<span style="margin-right: 0px;float:right">Avatar</span></h3></div>
                                <div>

                                    <form method="post" enctype="multipart/form-data">
                                        <img style="float: right" class="rounded-circle" width ="150px"src="public/image/avatar/<?=$user->avatar?>">
                                    <span style="float: right">
                                        <input type="file" name ="avatar"></br><h3>
                                        <button type="submit" name="changeAvatar">Đổi avatar</button></h3>

                                    </span>
                                    </form>

                                </div>
                                <div class="login">
                                    <div class="login_form_container">
                                        <div class="account_login_form">
                                            <form action="#" method="post" id = "formProfile">
                                                <div class="input-radio">
                                                    <span class="custom-radio"><input aria-selected="true" type="radio" value="1" name="gender" <?= $gender == 1?"checked='true'":""  ?>> Nam</span>
                                                    <span class="custom-radio"><input type="radio" value="0" name="gender" <?= $gender == 0?"checked='true'":""  ?> > Nữ </span>
                                                </div> <br>
                                                <label>Họ tên khách hàng</label>
                                                <input type="text" name="name" value="<?=$name?>" required>
                                                <label>Địa chỉ</label>
                                                <input type="text" name="address" value="<?=$address?>" required>
                                                <label>Số điện thoại</label>
                                                <input type="text" name="phone"  value="<?=$phone?>" required>
                                                <label>Email</label>
                                                <input type="email" name="email" value="<?=$email?>" required>
                                                <label>Birthdate</label>
                                                <input type="date"  value="<?= $DoB  ?>" name="DoB" required>
                                                <label>Password</label>
                                                <input type="password" name="password" id="password" required>
                                                <br>
                                                <div class="save_button primary_btn default_button">
                                                    <button type="submit" name ="submitProfile" id ="submitProfile">Lưu Lại</button>
                                                    <input type="hidden" name ="submitProfile" id ="submitProfile">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!--news letter popup start-->

<div class="newletter-popup" id ="newletter-popup">
    <div id="boxes" class="newletter-container">
        <div id="dialog" class="window" >
            <div id="popup2">
                <span class="b-close"><span>Đóng</span></span>
            </div>
            <div class="box">
                <div class="newletter-title " id ="newletter-title">

                    <h2>Chi tiết hóa đơn</h2>
                </div>
                <div class="box-content newleter-content">
                    <div class="table-responsive" id ="table12">
                        <button  style="margin-bottom: 10px;margin-right: 10px;"
                                 onclick=" PrintElem('table_bill') ;   " class ="btn btn-success">
                            In hóa đơn
                        </button>
                            <table class="table" id = "table_bill">
                            <thead>
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Hình ảnh</th>
                                <th>Số lượng</th>
                                <th>Đơn giá</th>
                                <th>Thành tiền</th>
                            </tr>
                            </thead>
                            <tbody id ="bill_detail">
                            <input type="hidden" id = "BillID" value="">
                            </tbody>
                        </table>

                    </div>
                    <!-- /#frm_subscribe -->
                </div>
                <!-- /.box-content -->
            </div>
        </div>

    </div>
    <!-- /.box -->
</div>

</div>
<!--news letter popup start-->


<script>
    function PrintElem(elem)
    {

        var mywindow = window.open('', 'PRINT', 'height=400,width=600');

        mywindow.document.write('<html><head>' +
            '<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha\n' +
            '    384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">' +
            '' +
            '' +
            '<title>' + "In hóa đơn"  + '</title>');
        mywindow.document.write('</head><body >');

        mywindow.document.write('<h1>' + "In hóa đơn"  + '</h1>');
        mywindow.document.write('<table class="table-bordered table" border="1px">');
        mywindow.document.write(document.getElementById(elem).innerHTML);
        mywindow.document.write('</table>');
        mywindow.document.write('</body></html>');
        // mywindow.document.close(); // necessary for IE >= 10
        // mywindow.focus(); // necessary for IE >= 10*/
        // mywindow.print();
        // mywindow.close();
        return true;
    }
        function setValue(id_bill){
            $.get("api/bill_detail_content.php",{
                'id_bill':id_bill,
            },function (data) {
                document.getElementById("bill_detail").innerHTML = data;
            });
                $('.newletter-popup').bPopup();
            // }, 0);
        }



            $("#submitProfile").click(function (e)
            {

                e.preventDefault();
                var pass = $("#password").val();
               $.post('api/checkPass.php',{
                   pass : pass
               },function (data)
               {
                   var objdata  = JSON.parse(data);
                    if(objdata.status == "success")
                    {
                        $("#submitProfile").val(1222);
                        $("#formProfile").submit();
                    }else{
                        swal("Mật khẩu không hợp lệ");
                        return;
                    }

               });
            })


</script>