<?php
include_once "models/m_product.php";
include_once "models/m_api.php";


class m_api extends database
{
    function AddToCart()
    {

        $cart = [];
        $cartUpdate = [];
        $productsID = [];
        $m_pro = new m_product();
        $products = $m_pro->showAllProducts();
        foreach ($products as $val)
        {
            $productsID[] = $val->ID;
        }
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        }
        if (isset($_POST['num'])) {
            $num = $_POST['num'];
        }
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
        }
        if (isset($_POST['cartUpdate'])) {
            $json = $_POST['cartUpdate'];
            $cartUpdate = json_decode($json, true);
        }

        if (isset($_COOKIE['cart'])) {

                $json = $_COOKIE['cart'];
                $cart = json_decode($json, true);
            for ($i = 0; $i < count($cart); $i++) {
                if ( !in_array($cart[$i]['id'],$productsID)) {
                    array_splice($cart, $i, 1);
                }
            }
            unset($_COOKIE['cart']);
            setcookie('cart', json_encode($cart), time() + 7 * 24 * 30 * 12, '/');

            switch ($action) {
                case 'add':
                    $isFind = false;
                    for ($i = 0; $i < count($cart); $i++) {
                        if ($cart[$i]['id'] == $id) {
                            $cart[$i]['num'] += $num;
                            $isFind = true;
                            break;
                        }
                    }
                    if (!$isFind) {
                        $cart[] = [
                            'id' => $id,
                            'num' => $num,
                        ];
                    }
                    setcookie('cart', json_encode($cart), time() + 7 * 24 * 30 * 12, '/');
                $this->QueryFromCart($cart);
//                var_dump($_SESSION);
                    break;
                case 'update':
                    setcookie('cart', json_encode($cartUpdate), time() + 7 * 24 * 30 * 12, '/');

                    $this->QueryFromCart($cartUpdate);
                    break;

                case 'delete':
                    for ($i = 0; $i < count($cart); $i++) {

                        if ($cart[$i]['id'] == $id) {
                            array_splice($cart, $i, 1);
                            unset($_COOKIE['cart']);
                            setcookie('cart', json_encode($cart), time() + 7 * 24 * 30 * 12, '/');
                            break;
                        }
                    }

                    $this->QueryFromCart($cart);

                    break;
            }
        } else {
            $id = $_POST['id'];
            array_push($cart, ['id' => $id, 'num' => $num]);
            setcookie('cart', json_encode($cart), time() + 7 * 24 * 30 * 12, '/');
            $this->QueryFromCart($cart);

        }



    }
    function QueryFromCart($cart = [])
    {
        if(isset($_SESSION['user'])){
            $user = $_SESSION['user'];
            $cart_user = 'cart'.$user->ID;
            if(isset($_SESSION[$cart_user]))
            {
                $_SESSION[$cart_user] = json_encode($cart);
            }
        }
        $listID = [];
            if (empty($cart)) {
                unset($_SESSION['cartList']);
                return;
            }
            foreach ($cart as $item) {
                $listID[] = $item['id'];
            }
            $sql = "SELECT * FROM san_pham WHERE ID in (" . implode($listID, ',') . ")";
            $m_pro = new m_product();
            $m_pro->setQuery($sql);
            $cartList = $m_pro->loadAllRows();
            $_SESSION['cartList'] = $cartList;

        }




}